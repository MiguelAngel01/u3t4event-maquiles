package dam.android.miguelangel.u3t4event;

import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

public class ActivityDatePickerFragment extends DialogFragment {

    private DatePicker dpDate;
    private Button btAcceptDate;
    private Button btCancelDate;
    private TextView tvDate;
    private String[] months;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dpDate = (DatePicker) view.findViewById(R.id.dpDate);
        btAcceptDate = (Button) view.findViewById(R.id.btAccept);
        btCancelDate = (Button) view.findViewById(R.id.btCancel);
        tvDate = (TextView) getActivity().findViewById(R.id.tvDate);

        dpDate.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                tvDate.setText(dayOfMonth + "-" + months[monthOfYear] + "-" + year);

            }
        });

        Resources res = getResources();
        months = res.getStringArray(R.array.months);

        if (!tvDate.getText().toString().equals("Fecha") && !tvDate.getText().toString().equals("Data") && !tvDate.getText().toString().equals("Date")) {
            String[] date = tvDate.getText().toString().split("-");

            int month = 0;
            while (!months[0].equals(date[1])) {
                month++;
            }
            setDate(Integer.parseInt(date[0]), month, Integer.parseInt(date[2]));
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.datefragment, container, false);
    }

    private String getDate() {
        return "Month: " + months[dpDate.getMonth()] + "\n" +
                "Day: " + dpDate.getDayOfMonth() + "\n" +
                "Year: " + dpDate.getYear();
    }

    private void setDate(int day, int month, int year){

        //dpDate.updateDate(year, month, day);

    }

}
