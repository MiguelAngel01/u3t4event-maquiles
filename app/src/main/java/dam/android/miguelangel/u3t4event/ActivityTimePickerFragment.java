package dam.android.miguelangel.u3t4event;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import org.w3c.dom.Text;

public class ActivityTimePickerFragment extends DialogFragment {

    private TimePicker tpTime;
    private Button btAcceptTime;
    private Button btCancelTime;
    private TextView tvTime;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tpTime = (TimePicker) view.findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAcceptTime = (Button) view.findViewById(R.id.btAccept);
        btCancelTime = (Button) view.findViewById(R.id.btCancel);
        tvTime = (TextView) getActivity().findViewById(R.id.tvTime);

        if (!tvTime.getText().toString().equals("Hora") && !tvTime.getText().toString().equals("Time")) {
            String[] time = tvTime.getText().toString().split(":");
            setTime(Integer.parseInt(time[0]), Integer.parseInt(time[1]));
        }

        tpTime.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                tvTime.setText(getTime());
            }
        });

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.timefragment, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public String getTime() {
        return tpTime.getHour() + ":" + tpTime.getMinute();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void setTime(int hour, int min){
        tpTime.setHour(hour);
        tpTime.setMinute(min);
    }

}