package dam.android.miguelangel.u3t4event;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import java.util.Calendar;

public class EventDataActivity extends AppCompatActivity
        implements View.OnClickListener, RadioGroup.OnCheckedChangeListener{

    private String priority = "Normal";

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private Button btAccept;
    private Button btCancel;
    private TextView tvPlace;
    private EditText etPlace;
    private TextView tvDate;
    private TextView tvTime;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_data_activity);

        setUI();

        Bundle inputData = getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setUI() {

        Bundle inputData = getIntent().getExtras();

        tvEventName = (TextView) findViewById(R.id.tvEventName);
        rgPriority = (RadioGroup) findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        btAccept = (Button) findViewById(R.id.btAccept);
        btCancel = (Button) findViewById(R.id.btCancel);
        tvPlace = (TextView) findViewById(R.id.tvPlace);
        etPlace = (EditText) findViewById(R.id.etPlace);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvTime = (TextView) findViewById(R.id.tvTime);


        if(!inputData.getString("EventDataOriginal").equals("none")){
            String[] lines = inputData.getString("EventDataOriginal").split("\n");
            String[] data = new String[lines.length];

            for (int i = 0; i < lines.length; i++) {
                String[] aux = lines[i].split(" ");
                data[i] = aux[1];
            }

            String priority = "rb" + data[0];
            switch (priority) {
                case "rbLow":
                    rgPriority.check(R.id.rbLow);
                    break;
                case "rbNormal":
                    rgPriority.check(R.id.rbNormal);
                    break;
                case "rbHigh":
                    rgPriority.check(R.id.rbHigh);
                    break;
            }

            etPlace.setText(data[1]);

            tvDate.setText(data[2]);
            tvTime.setText(data[3]);

        }

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();
        Bundle inputData = getIntent().getExtras();

        switch (v.getId()) {
            case R.id.btAccept:
                eventData.putString("EventData", "Priority: " + priority + "\n" +
                                                  "Place: " + etPlace.getText().toString() + "\nDate: " + tvDate.getText().toString() + "\nHour: " + tvTime.getText().toString());
                break;
            case R.id.btCancel:
                eventData.putString("EventData", inputData.getString("EventDataOriginal"));
                break;
        }

        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int i) {

        switch (i) {
            case R.id.rbLow:
                priority = "Low";
                break;
            case R.id.rbNormal:
                priority = "Normal";
                break;
            case R.id.rbHigh:
                priority = "High";
                break;
        }

    }

    public void showDateDialog(View view) {

        ActivityDatePickerFragment dpFragment = new ActivityDatePickerFragment();
        dpFragment.setCancelable(true);
        dpFragment.show(getSupportFragmentManager(), "Date Picker");

    }

    public void showTimeDialog(View view) {

        ActivityTimePickerFragment tpFragment = new ActivityTimePickerFragment();
        tpFragment.setCancelable(true);
        tpFragment.show(getSupportFragmentManager(), "Date Picker");

    }

    public void onClickFragmentDate(View v){

        switch (v.getId()) {
            case R.id.btAcceptDate:
                break;
            case R.id.btCancelDate:
                tvDate.setText("Fecha");
                break;
        }

        //onBackPressed();

    }

    public void onClickFragmentTime(View v) {

        switch (v.getId()) {
            case R.id.btAcceptTime:
                break;
            case R.id.btCancelTime:
                tvTime.setText("Place");
                break;
        }

        //onBackPressed();

    }

}
