package dam.android.miguelangel.u3t4event;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;

public class EventDataActivity extends AppCompatActivity
        implements View.OnClickListener, RadioGroup.OnCheckedChangeListener{

    private String priority = "Normal";

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;
    private TextView tvPlace;
    private EditText etPlace;
    private String[] months;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_data_activity);

        setUI();

        Resources res = getResources();
        months = res.getStringArray(R.array.months);

        Bundle inputData = getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setUI() {

        Bundle inputData = getIntent().getExtras();

        tvEventName = (TextView) findViewById(R.id.tvEventName);
        rgPriority = (RadioGroup) findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        dpDate = (DatePicker) findViewById(R.id.dpDate);
        tpTime = (TimePicker) findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept = (Button) findViewById(R.id.btAccept);
        btCancel = (Button) findViewById(R.id.btCancel);
        tvPlace = (TextView) findViewById(R.id.tvPlace);
        etPlace = (EditText) findViewById(R.id.etPlace);

        if(!inputData.getString("EventDataOriginal").equals("none")){
            String[] lines = inputData.getString("EventDataOriginal").split("\n");
            String[] data = new String[lines.length];

            for (int i = 0; i < lines.length; i++) {
                String[] aux = lines[i].split(" ");
                data[i] = aux[1];
            }

            String priority = "rb" + data[0];
            switch (priority) {
                case "rbLow":
                    rgPriority.check(R.id.rbLow);
                    break;
                case "rbNormal":
                    rgPriority.check(R.id.rbNormal);
                    break;
                case "rbHigh":
                    rgPriority.check(R.id.rbHigh);
                    break;
            }

            etPlace.setText(data[1]);

            String day = data[3];
            String month = data[2];
            String year = data[4];

            //Seria así pero actualmente el metodo setCalendarViewShown esta obsoleto y solo se puede hacer desde xml
            /*if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                dpDate.setCalendarViewShown(false);
            } else {
                dpDate.setCalendarViewShown(true);
            }*/

            //dpDate.updateDate(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));

            String[] hour = data[5].split(":");
            tpTime.setHour(Integer.parseInt(hour[0]));
            tpTime.setMinute(Integer.parseInt(hour[1]));

        }

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();
        Bundle inputData = getIntent().getExtras();

        switch (v.getId()) {
            case R.id.btAccept:
                eventData.putString("EventData", "Priority: " + priority + "\n" +
                                                  "Place: " + etPlace.getText().toString() + "\n" +
                                                  "Month: " + months[dpDate.getMonth()] + "\n" +
                                                  "Day: " + dpDate.getDayOfMonth() + "\n" +
                                                  "Year: " + dpDate.getYear() + "\n" +
                                                  "Hour: " + tpTime.getHour() + ":" + tpTime.getMinute());
                break;
            case R.id.btCancel:
                eventData.putString("EventData", inputData.getString("EventDataOriginal"));
                break;
        }

        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int i) {

        switch (i) {
            case R.id.rbLow:
                priority = "Low";
                break;
            case R.id.rbNormal:
                priority = "Normal";
                break;
            case R.id.rbHigh:
                priority = "High";
                break;
        }

    }
}
